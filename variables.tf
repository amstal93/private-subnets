variable "availability_zones" {
  description = "AWS Availability Zones"
  type = list
}

variable "name" {
  description = "Application Name"
  type = string
}

variable "start_ip" {
  description = "Start ip of private subnets"
  type = string
}

variable "subnet_prefix" {
  description = "Prefix ip for setup of subnets"
  type = string
}

variable "vpc_id" {
  description = "AWS VPC Id"
  type = string
}

data "aws_vpc" "selected" {
  id = var.vpc_id
}

locals {
  count = length(var.availability_zones)
}