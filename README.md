# Terraform Private Subnets Module

It creates a private subnet on each Availability Zone on variables.

## Contains

- Private Subnets
- Route Table Association

## Depends on

- [VPC Module](https://gitlab.com/terraform147/vpc)

## Outputs

- Private Subnets Id's

## How to use

### Setup Module

```
module "private_subnet" {
  source = "git@gitlab.com:terraform147/private-subnets.git"
  name = ""
  vpc_id = ""
  subnet_prefix = ""
  start_ip = ""
  availability_zones = ""
}
```

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: